Assignment 3- Pattern Searching
Names:
	Andrew Williamson- 1215027
	Vishnu Priya Mallela- 1166608

Compile:
javac -cp src -d bin src/Parser.java

Run:
java -cp bin Parser


The parser constructs an nfsa, then uses the subset method to convert it to a dfsa.
The DFSA is output in Dot format at the end of the search, and can be viewed at http://sandbox.kidstrythisathome.com/erdos/
Or you can hide stderr (append "2> null" to the run command) to hide the graph.



Regular grammar(Defined in the Parser class):

E -> T
E -> T|E
T -> S
T -> ST
S -> F*
S -> F?
F -> (E)
F -> [C]
F -> []C]
F -> []]
F -> .
F -> L
L -> \ anything
L -> anything but ()[]|*?.
C -> anything but ]


