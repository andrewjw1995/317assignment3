import static org.junit.Assert.*;

import java.io.StringReader;
import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class ParserTest
{
	@Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]
        	{     
                 { "abc", true },
                 { "abs)", false },
                 { "[]", false },
                 { "[a*b|ac]d", true },
                 { "\\", false },
                 { "\\\\", true },
                 {"\\|sg*d?|sdsa", true},
                 {"sg*d?|sdsa", true},
                 {"abc|cba", true},
                 {"(abc)|(cba)", true},
                 {".g*d?|sdsa", true},
                 {"[abc]", true},
                 {"a*bde*c", true},
                 {"a?bde?c", true},
                 {"a.bde.c", true},
                 {"abd|ec", true},
                 {"abd[ec]c", true},
                 {"abd\\[ec", true},
                 {"abd\\[ec", true},
                 {"abd\\ec", true},
                 {"(a*b?(a|b)c.\\))|c*d", true},
                 {"ab|cd|ef", true},
                 {"abc[]]", true},
                 {"abc[[]abc", true},
                 {"abc[]abc]abc", true},
                 {"abc[+bc|]abc", true},
                 {"[]", false},
                 {"sg**d?|sdsa", false},
                 {"sg*d??|sdsa", false},
                 {"sg*d?||sdsa", false},
                 {"|sg*d?|sdsa", false},
                 {"*ac", false},
                 {"?ad", false},
                 {"|c", false},
                 {"a[]b", false},
                 {"a()b", false}
           });
    }

    private Parser parser;
    private String regex;
    private boolean expected;

    public ParserTest(String regex, boolean expected) {
    	this.regex = regex;
        this.parser = new Parser(new StringReader(regex));
        this.expected = expected;
    }

    @Test
    public void test()
    {
    	try
    	{
    		parser.parse();
    		if (!expected)
    			fail("Did not throw an exception on an invalid regex: " + regex);
    	}
    	catch(Exception e)
    	{
    		if (expected)
    			fail("Threw an exception on a valid regex: " + regex + " " + e.getMessage());
    	}
    }
}
