import static org.junit.Assert.*;

import java.util.Deque;
import java.util.Iterator;
import java.util.NoSuchElementException;

import org.junit.Test;


// Relies on auto boxing using cached integers for range 1 - 64
public class ArrayDequeTest
{

	@Test
	public void testAdd()
	{
		Deque<Integer> deque = new ArrayDeque<Integer>();
		for (int i = 1; i <= 64; i++)
		{
			deque.add(i);
			assertSame(i, deque.size());
			assertSame(i, deque.peekFirst());
		}
	}

	@Test
	public void testAddFirst()
	{
		Deque<Integer> deque = new ArrayDeque<Integer>();
		for (int i = 1; i <= 64; i++)
		{
			deque.addFirst(i);
			assertSame(i, deque.size());
			assertSame(i, deque.peekFirst());
		}
	}

	@Test
	public void testAddLast()
	{
		Deque<Integer> deque = new ArrayDeque<Integer>();
		for (int i = 1; i <= 64; i++)
		{
			deque.addLast(i);
			assertSame(i, deque.size());
			assertSame(i, deque.peekLast());
		}
	}
	
	@Test
	public void testRemove()
	{
		Deque<Integer> deque = new ArrayDeque<Integer>();
		for (int i = 1; i <= 64; i++)
			deque.add(i);
		
		for (int i = 64; i > 0; i--)
			assertSame(i, deque.remove());
		
		try
		{
			deque.remove();
			fail("Expected the deque to be empty, and to throw an exception");
		}
		catch(NoSuchElementException e) { }
	}
	
	@Test
	public void testRemoveFirst()
	{
		Deque<Integer> deque = new ArrayDeque<Integer>();
		for (int i = 1; i <= 64; i++)
			deque.add(i);
		
		for (int i = 64; i > 0; i--)
			assertSame(i, deque.removeFirst());
		
		try
		{
			deque.removeFirst();
			fail("Expected the deque to be empty, and to throw an exception");
		}
		catch(NoSuchElementException e) { }
	}

	@Test
	public void testRemoveLast()
	{
		Deque<Integer> deque = new ArrayDeque<Integer>();
		for (int i = 1; i <= 64; i++)
			deque.add(i);
		
		for (int i = 1; i <= 64; i++)
			assertSame(i, deque.removeLast());
		
		try
		{
			deque.removeLast();
			fail("Expected the deque to be empty, and to throw an exception");
		}
		catch(NoSuchElementException e) { }
	}
	
	@Test
	public void testIterator()
	{
		Deque<Integer> deque = new ArrayDeque<Integer>();
		for (int i = 1; i <= 64; i++)
			deque.addLast(i);
		
		Iterator<Integer> iterator = deque.iterator();
		for (int i = 1; i <= 64; i++)
			assertSame(i, iterator.next());
		
		assertFalse(iterator.hasNext());
	}
	
	@Test
	public void testContains()
	{
		Deque<Integer> deque = new ArrayDeque<Integer>();
		for (int i = 1; i <= 64; i++)
			deque.addLast(i);
		
		assertTrue(deque.contains(24));
		assertFalse(deque.contains(65));
	}
	
	@Test
	public void testToArrayImplicit()
	{
		Deque<Integer> deque = new ArrayDeque<Integer>();
		Integer[] expected = new Integer[64];
		for (int i = 1; i <= 64; i++)
		{
			deque.addLast(i);
			expected[i - 1] = i;
		}
		
		Object[] actual = deque.toArray();
		assertArrayEquals(expected, actual);
	}
	
	@Test
	public void testToArrayExplicit()
	{
		Deque<Integer> deque = new ArrayDeque<Integer>();
		Integer[] expected = new Integer[64];
		for (int i = 1; i <= 64; i++)
		{
			deque.addLast(i);
			expected[i - 1] = i;
		}
		
		Integer[] actual = new Integer[5];
		actual = deque.toArray(actual);
		assertArrayEquals(expected, actual);
	}
	
	@Test
	public void testRemoveFirstOccurrence()
	{
		Deque<Integer> deque = new ArrayDeque<Integer>();
		for (int i = 1; i <= 64; i++)
			deque.addLast(i);
		
		assertTrue(deque.removeFirstOccurrence(3));
		assertFalse(deque.contains(3));
		assertFalse(deque.removeFirstOccurrence(65));
	}

	@Test
	public void testRemoveLastOccurrence()
	{
		Deque<Integer> deque = new ArrayDeque<Integer>();
		for (int i = 1; i <= 64; i++)
			deque.addLast(i);
		
		assertTrue(deque.removeLastOccurrence(3));
		assertFalse(deque.contains(3));
		assertFalse(deque.removeLastOccurrence(65));
	}
}
