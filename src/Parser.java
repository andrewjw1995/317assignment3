import java.io.*;

public class Parser
{
	FSA states;
	int next;
	Reader input;

	public Parser(Reader input)
	{
		this.input = input;
		this.states = new FSA();
	}
	
	public FSA parse() throws Exception
	{
		next = input.read();
		State start = states.createNode();
		State end = states.createNode();
		end.terminal = true;
		expression(start, end);
		if (next != -1)
			throw new Exception("Closing parenthesis has no matching opening parenthesis");
		return states;
	}

	private void expression(State start, State end) throws Exception
	{
		term(start, end);
		if (next == '|')
		{
			next = input.read();
			expression(start, end);
		}
	}

	private void term(State start, State end) throws Exception
	{
		State mid = states.createNode();
		subterm(start, mid);
		if (next != -1 && next != ')' && next != '|')
			term(mid, end);
		else
			states.addEdge(mid, '\0', end);
	}

	private void subterm(State start, State end) throws Exception
	{
		factor(start, end);
		if (next == '*')
		{
			states.addEdge(start, '\0', end);
			states.addEdge(end, '\0', start);
			next = input.read();
		}
		else if (next == '?')
		{
			states.addEdge(start, '\0', end);
			next = input.read();
		}
	}

	private void factor(State start, State end) throws Exception
	{
		if (next == '(')
		{
			next = input.read();
			expression(start, end);
			if (next == ')')
				next = input.read();
			else
				throw new Exception("Expected closing parenthesis \")\"");
		}
		else if (next == '[')
		{
			next = input.read();
			if (next == ']')
			{
				states.addEdge(start, ']', end);
				next = input.read();
				if (next == ']')
					next = input.read();
				else
					character(start, end);
			}
			else if (next != -1)
			{
				character(start, end);
			}
			else
				throw new Exception("Could not find closing square brace - Note that a list of values within square braces [] cannot be empty");
		}
		else if (next == '.')
		{
			next = input.read();
			for (char c = 1; c < 256; c++)
				states.addEdge(start, c, end);
		}
		else
			literal(start, end);
	}
	
	private void character(State start, State end) throws Exception
	{
		if (next == ']')
			next = input.read();
		else if (next != -1)
		{
			states.addEdge(start, (char)next, end);
			next = input.read();
			character(start, end);
		}
		else
			throw new Exception("Badly formed literal list");
	}

	private void literal(State start, State end) throws Exception
	{
		if (next == -1)
			throw new Exception("End of regex reached unexpectedly");
		else if(next == '\\')
		{
			next = input.read();
			if (next == -1)
				throw new Exception("Expected escaped symbol, but reached end of regex");
			states.addEdge(start, (char)next, end);
			next = input.read();
		}
		else if (".?|*[]()".indexOf(next) == -1)
		{
			states.addEdge(start, (char)next, end);
			next = input.read();
		}
		else
			throw new Exception("Badly formed input at symbol " + (char) next);
	}
	
	public static void main(String[] args)
	{
		if (args.length != 2)
		{
			System.out.println("Usage: Parser [regex] [file]");
			return;
		}
		File file = new File(args[1]);
		if (!file.exists())
		{
			System.out.println(args[1] + " does not exist");
			return;
		}
		if (!file.isFile())
		{
			System.out.println(args[1] + " is not a file");
			return;
		}
		
		FSA regex = null;
		try
		{
			Parser parser = new Parser(new StringReader(args[0]));
			regex = parser.parse().toDFSA();
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
			return;
		}
		
		try
		{
			BufferedReader reader = new BufferedReader(new FileReader(file));
			String line = reader.readLine();
			while (line != null)
			{
				if (regex.search(line) != null)
					System.err.println(line);
				line = reader.readLine();
			}
			reader.close();
			System.out.println(regex.toDotFormat("DFSA"));
		}
		catch(IOException e)
		{
			System.out.println("Fatal exception: " + e);
			System.exit(-1);
		}
	}
}
