import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.Stack;


public class FSA
{
	List<Transition> edges = new ArrayList<>();
	List<State> states = new ArrayList<>();

	public void addNode(State value)
	{
		states.add(value);
	}

	public void addEdge(Transition value)
	{
		edges.add(value);
	}

	public Collection<State> nodes()
	{
		return states;
	}

	public Collection<Transition> edges()
	{
		return edges;
	}

	public int totalNodes()
	{
		return states.size();
	}

	public int totalEdges()
	{
		return edges.size();
	}
	
	public State createNode()
	{
		State n = new State(states.size());
		states.add(n);
		return n;
	}
	
	public Transition addEdge(State from, char c, State to)
	{
		Transition e = new Transition(from, c, to);
		edges.add(e);
		return e;
	}
	
	public State getNode(int id)
	{
		for (State state : states)
			if (state.id == id)
				return state;
		throw new NoSuchElementException();
	}
	
	// Useful utility method for exporting the gaph in a common format.
	// There are many tools that support loading DOT format graphs.
	// Graphs can be viewed online at http://graphviz-dev.appspot.com/
	public String toDotFormat(String name)
	{
		StringBuilder builder = new StringBuilder("digraph ");
		builder.append(name);
		builder.append(" { \n");
		for (State state : states)
		{
			builder.append(state.id);
			if (state.terminal)
				builder.append(" [color=blue]");
			builder.append(";\n");
		}
		builder.append('\n');
		for (Transition edge : edges)
		{
			builder.append(edge.from);
			builder.append(" -> ");
			builder.append(edge.to);
			builder.append(" [label=\"");
			builder.append(edge.label == '\0' ? 'λ' : edge.label);
			builder.append("\"];\n");
		}
		builder.append("}");
		return builder.toString();
	}
	
	public FSA toDFSA()
	{
		FSA result = new FSA();
		HashMap<Set<State>, State> states = new HashMap<>();
		Stack<Set<State>> unexplored = new Stack<>();
		
		HashSet<State> initial = new HashSet<State>();
		State start = this.states.get(0);
		initial.add(start);
		closure(start, initial);
		unexplored.push(initial);
		states.put(initial, result.createNode());
		
		while(unexplored.size() > 0)
		{
			Set<State> set = unexplored.pop();
			State parent = states.get(set);
			for (State state : set)
			{
				if (state.terminal)
				{
					parent.terminal = true;
					break;
				}
			}
					
			
			for (char c = 1; c < 256; c++)
			{
				Set<State> next = explore(set, c);
				if (next.size() == 0)
					continue;
				State child;
				if (states.containsKey(next))
				{
					child = states.get(next);
				}
				else
				{
					child = result.createNode();
					states.put(next, child);
					unexplored.push(next);
				}
				result.addEdge(parent, c, child);
			}
		}
		return result;
	}
	
	public Set<State> explore(Set<State> nodes, char label)
	{
		Set<State> result = new HashSet<>();
		Set<State> closure = new HashSet<>();
		move(nodes, label, result);
		for (State node : result)
			closure(node, closure);
		result.addAll(closure);
		return result;
	}
	
	public void closure(State node, Set<State> result)
	{
		for (Transition edge : edges)
			if (edge.from == node && edge.label == '\0' && result.add(edge.to))
				closure(edge.to, result);
	}
	
	public void move(Set<State> nodes, char label, Set<State> result)
	{
		for (Transition edge : edges)
			if (edge.label == label && nodes.contains(edge.from))
				result.add(edge.to);
	}
	
	public String search(String source)
	{
		char[] chars = source.toCharArray();
		HashSet<State> current = new HashSet<>();
		HashSet<State> next = new HashSet<>();
		
		for (int position = 0; position < chars.length; position++)
		{
			int length = position;
			current.add(states.get(0));
			while(length < chars.length && current.size() > 0)
			{
				for (Transition edge : edges)
				{
					if (edge.label == chars[length] && current.contains(edge.from))
					{
						if (edge.to.terminal)
							return source.substring(position, length + 1);
						next.add(edge.to);
					}
				}
				current.clear();
				HashSet<State> swap = current;
				current = next;
				next = swap;
				length++;
			}
		}
		return null;
	}
}
