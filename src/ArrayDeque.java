import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.ConcurrentModificationException;
import java.util.Deque;
import java.util.Iterator;
import java.util.NoSuchElementException;


public class ArrayDeque<E> implements Deque<E>
{
	private Object[] buffer;
	private int head;
	private int size;
	private int action;
	
	private class ForwardIterator implements Iterator<E>
	{
		private final ArrayDeque<E> parent;
		private final int action;
		private int current = 0;
		
		public ForwardIterator(ArrayDeque<E> parent)
		{
			this.parent = parent;
			this.action = parent.action;
		}
		
		@Override
		public boolean hasNext()
		{
			if (parent.action != action)
				throw new ConcurrentModificationException();
			return current != parent.size;
		}

		@SuppressWarnings("unchecked")
		@Override
		public E next()
		{
			if (parent.action != action)
				throw new ConcurrentModificationException();
			int index = (head + current) % parent.buffer.length;
			E element = (E) parent.buffer[index];
			current++;
			return element;
		}

		@Override
		public void remove()
		{
			throw new UnsupportedOperationException();
		}
	}
	
	private class ReverseIterator implements Iterator<E>
	{
		private final ArrayDeque<E> parent;
		private final int action;
		private int current;
		
		public ReverseIterator(ArrayDeque<E> parent)
		{
			this.parent = parent;
			this.action = parent.action;
			current = parent.buffer.length;
		}
		
		@Override
		public boolean hasNext()
		{
			if (parent.action != action)
				throw new ConcurrentModificationException();
			return current != 0;
		}

		@SuppressWarnings("unchecked")
		@Override
		public E next()
		{
			if (parent.action != action)
				throw new ConcurrentModificationException();
			int index = (head + current) % parent.buffer.length;
			E element = (E) parent.buffer[index];
			current--;
			return element;
		}

		@Override
		public void remove()
		{
			throw new UnsupportedOperationException();
		}
	}
	
	public ArrayDeque()
	{
		buffer = new Object[16];
	}

	@Override
	public boolean addAll(Collection<? extends E> collection)
	{
		boolean modified = false;
		for (E element : collection)
			modified |= add(element);
		return modified;
	}

	@Override
	public void clear()
	{
		action++;
		size = 0;
	}

	@Override
	public boolean containsAll(Collection<?> collection)
	{
		for (Object element : collection)
			if (!contains(element))
				return false;
		return true;
	}

	@Override
	public boolean isEmpty()
	{
		return size == 0;
	}

	@Override
	public boolean removeAll(Collection<?> collection)
	{
		boolean modified = false;
		for (Object element : collection)
			modified |= remove(element);
		return modified;
	}

	@Override
	public boolean retainAll(Collection<?> collection)
	{
		Collection<E> removals = new ArrayList<E>();
		for (E obj : this)
			if (!collection.contains(obj))
				removals.add(obj);
		return removeAll(removals);
	}

	@Override
	public Object[] toArray()
	{
		Object[] result = new Object[size];
		int tail = head + size;
		if (tail > buffer.length)
		{
			int overflow = tail % buffer.length;
			System.arraycopy(buffer, head, result, 0, size - overflow);
			System.arraycopy(buffer, 0, result, size - overflow, overflow);
		}
		else
			System.arraycopy(buffer, head, result, 0, size);
		return result;
	}

	@Override
	public <T> T[] toArray(T[] a)
	{
		T[] result = a;
		if (result.length < size)
			result = Arrays.copyOf(result, size);
		int tail = head + size;
		if (tail > buffer.length)
		{
			int overflow = tail % buffer.length;
			System.arraycopy(buffer, head, result, 0, size - overflow);
			System.arraycopy(buffer, 0, result, size - overflow, overflow);
		}
		else
			System.arraycopy(buffer, head, result, 0, size);
		return result;
	}

	@Override
	public boolean add(E element)
	{
		addFirst(element);
		return true;
	}

	@Override
	public void addFirst(E element)
	{
		if (size == buffer.length)
			expand();
		head = (head + buffer.length - 1) % buffer.length;
		buffer[head] = element;
		size++;
		action++;
	}

	@Override
	public void addLast(E element)
	{
		if (size == buffer.length)
			expand();
		int tail = head + size;
		buffer[tail] = element;
		size++;
		action++;
	}

	@Override
	public boolean contains(Object other)
	{
		for (Object element : this)
			if (element.equals(other))
				return true;
		return false;
	}

	@Override
	public Iterator<E> descendingIterator()
	{
		return new ReverseIterator(this);
	}

	@Override
	public E element()
	{
		return getFirst();
	}

	@SuppressWarnings("unchecked")
	@Override
	public E getFirst()
	{
		if (size == 0)
			throw new NoSuchElementException();
		return (E) buffer[head];
	}

	@SuppressWarnings("unchecked")
	@Override
	public E getLast()
	{
		if (size == 0)
			throw new NoSuchElementException();
		int tail = (head + size - 1) % buffer.length;
		return (E) buffer[tail];
	}

	@Override
	public Iterator<E> iterator()
	{
		return new ForwardIterator(this);
	}

	@Override
	public boolean offer(E element)
	{
		return offerFirst(element);
	}

	@Override
	public boolean offerFirst(E element)
	{
		if (size == buffer.length)
			return false;
		head = (head - 1) % buffer.length;
		buffer[head] = element;
		size++;
		action++;
		return true;
	}

	@Override
	public boolean offerLast(E element)
	{
		if (size == buffer.length)
			return false;
		int tail = (head + size) % buffer.length;
		buffer[tail] = element;
		size++;
		action++;
		return true;
	}

	@Override
	public E peek()
	{
		return peekFirst();
	}

	@SuppressWarnings("unchecked")
	@Override
	public E peekFirst()
	{
		if (size == 0)
			return null;
		return (E) buffer[head];
	}

	@SuppressWarnings("unchecked")
	@Override
	public E peekLast()
	{
		if (size == 0)
			return null;
		int tail = (head + size - 1) % buffer.length;
		return (E) buffer[tail];
	}

	@Override
	public E poll()
	{
		return pollFirst();
	}

	@SuppressWarnings("unchecked")
	@Override
	public E pollFirst()
	{
		if (size == 0)
			return null;
		E element = (E) buffer[head];
		head = (head + 1) % buffer.length;
		size--;
		action++;
		return element;
	}

	@Override
	public E pollLast()
	{
		if (size == 0)
			return null;
		int tail = (head + size - 1) % buffer.length;
		@SuppressWarnings("unchecked")
		E element = (E) buffer[tail];
		size--;
		action++;
		return element;
	}

	@Override
	public E pop()
	{
		return removeFirst();
	}

	@Override
	public void push(E element)
	{
		addFirst(element);
	}

	@Override
	public E remove()
	{
		return removeFirst();
	}

	@Override
	public boolean remove(Object obj)
	{
		return removeFirstOccurrence(obj);
	}

	@Override
	public E removeFirst()
	{
		if (size == 0)
			throw new NoSuchElementException();
		@SuppressWarnings("unchecked")
		E element = (E) buffer[head];
		head = (head + 1) % buffer.length;
		size--;
		action++;
		return element;
	}

	@Override
	public boolean removeFirstOccurrence(Object obj)
	{
		boolean modified = false;
		for (int i = 0; i < size; i++)
		{
			int index = (head + i) % buffer.length;
			int next = (head + i + 1) % buffer.length;
			if (modified)
				buffer[index] = buffer[next];
			else if (buffer[index].equals(obj))
			{
				modified = true;
				size--;
				action++;
				buffer[index] = buffer[next];
			}
		}
		return modified;
	}

	@Override
	public E removeLast()
	{
		if (size == 0)
			throw new NoSuchElementException();
		int tail = (head + size - 1) % buffer.length;
		@SuppressWarnings("unchecked")
		E element = (E) buffer[tail];
		size--;
		action++;
		return element;
	}

	@Override
	public boolean removeLastOccurrence(Object obj)
	{
		boolean modified = false;
		for (int i = size - 1; i >= 0; i--)
		{
			if (buffer[(head + i) % buffer.length].equals(obj))
			{
				modified = true;
				size--;
				action++;
				for (int j = i; j < size; j++)
				{
					int index = (head + j) % buffer.length;
					int next = (head + j + 1) % buffer.length;
					buffer[index] = buffer[next];
				}
			}
		}
		return modified;
	}

	@Override
	public int size()
	{
		return size;
	}
	
	private void expand()
	{
		Object[] old = buffer;
		buffer = new Object[old.length * 2];
		int tail = head + size;
		if (tail > old.length)
		{
			int overflow = tail % old.length;
			int contiguous = size - overflow;
			System.arraycopy(old, head, buffer, head, contiguous);
			System.arraycopy(old, 0, buffer, head + contiguous, overflow);
		}
		else
			System.arraycopy(old, head, buffer, head, size);
	}
}
