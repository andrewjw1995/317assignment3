public class State
{
	int id;
	boolean terminal;
	
	public State(int id)
	{
		this.id = id;
	}
	
	public String id()
	{
		return Integer.toString(id);
	}
	
	public String toString()
	{
		return Integer.toString(id);
	}
}
