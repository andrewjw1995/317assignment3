public class Transition
{
	State from;
	char label;
	State to;
	
	public Transition(State f, char c, State t)
	{
		from = f;
		label = c;
		to = t;
	}
	
	public String id()
	{
		return from.id() + "->" + to.id();
	}

	public State from()
	{
		return from;
	}

	public State to()
	{
		return to;
	}
}
